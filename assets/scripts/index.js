import getElements from './helpers/get_elements';

import list from './templates/list';

import '../styles/style.css';

let START_TIME = true;
let TIME = 0;
let INTERVAL;
let RECORD_TIME = [];
const TIME_FORMAT = 100;

const timeBox = getElements('sw-time-box-js');
timeBox.innerHTML = TIME / TIME_FORMAT;

const recordTimeBox = getElements('sw-time-recorded-js');

const startTimeClicked = () => {
  if (START_TIME) {
    INTERVAL = setInterval((() => {
      TIME++;
      return timeBox.innerHTML = TIME / TIME_FORMAT;
    }), 10);
    START_TIME = false;
    return;
  }

  START_TIME = true;
  clearTimeout(INTERVAL);
};

const resetTimeClicked = () => {
  TIME = 0;
  START_TIME = true;
  RECORD_TIME = [];
  clearTimeout(INTERVAL);
  timeBox.innerHTML = TIME;
  recordTimeBox.innerHTML = RECORD_TIME;
  return;
};

const recordTimetClicked = () => {
  RECORD_TIME.push(TIME / TIME_FORMAT);
  const recordTimeList = list(RECORD_TIME);
  recordTimeBox.innerHTML = recordTimeList;
  return;
};

const startTimeEvent = getElements('sw-time-start-js');
startTimeEvent.addEventListener('click', () => startTimeClicked());

const resetTimetEvent = getElements('sw-time-reset-js');
resetTimetEvent.addEventListener('click', () => resetTimeClicked());

const recordTimetEvent = getElements('sw-time-record-js');
recordTimetEvent.addEventListener('click', () => recordTimetClicked());

document.addEventListener('keydown', event => {
  switch(event.keyCode) {
    case 82: resetTimeClicked();
    break;
    case 83: startTimeClicked();
    break;
    case 84: recordTimetClicked();
    break;
  }
});
