import '../../styles/templates/list.css';

const list = items =>
  `<ul class='sw-list'>
    ${
      Array.from(items,
        item => `<li class='sw-list--item'>${item}</li>`).join('')
    }
  </ul>`;

export default list;
